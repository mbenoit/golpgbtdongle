package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"

	"BNL.gov/golpgbtdongle"
)

func Splash() string {

	splash := "\n-------------------------------------------------------------------------------\n"
	splash += "|                    lpGBT Dongle Command Line Interface                      |\n"
	splash += "|                  Author : Mathieu Benoit mbenoit@bnl.gov                    |\n"
	splash += "|                 designed for lpGBTv1, ALFE2 and CERN I2C Dongle             |\n"
	splash += "-------------------------------------------------------------------------------\n"

	return splash
}

func handleExit() {
	rawModeOff := exec.Command("/bin/stty", "-raw", "echo")
	rawModeOff.Stdin = os.Stdin
	_ = rawModeOff.Run()
	rawModeOff.Wait()
}

func Run(gbtxaddr, alfe2addr, master int, dryrun, verbose bool) {

	golpgbtdongle.CLIloop(gbtxaddr, alfe2addr, master, dryrun, verbose)

}

func main() {
	defer handleExit()
	//-------------- Parse ------------------------------------------//
	gbtxaddr := flag.Int("g", 127, "lpGBT I2C Address")
	alfe2addr := flag.Int("a", 0x0, "ALFE2 I2C Address")
	alfe2bus := flag.Int("b", 0x0, "ALFE2 I2C Master on lpGBT")
	dryrun := flag.Bool("d", false, "Perform Dry-Run")
	verbose := flag.Bool("v", false, "Enable more verbosity")

	flag.Parse()
	fmt.Print(Splash())
	Run(*gbtxaddr, *alfe2addr, *alfe2bus, *dryrun, *verbose)

}
