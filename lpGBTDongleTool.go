package main

import (
	"flag"

	"BNL.gov/golpgbtdongle"
)

func main() {

	devaddr := flag.Int("a", 127, "I2C base address of the device")
	conffile := flag.String("f", "lpGBTv1.conf", "TXT file containing configuration for the lpGBT chip")
	dryrun := flag.Bool("dry", false, "Dry run reading yaml file")
	verbose := flag.Bool("v", true, "print extra verbosity during execution")

	flag.Parse()

	gbtx := golpgbtdongle.GBTx{GBTx_address: *devaddr}
	gbtx.Init(*dryrun, *verbose)
	gbtx.GBTx_Dump_Config(*conffile)

}
