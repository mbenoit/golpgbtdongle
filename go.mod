module main

go 1.17

replace BNL.gov/golpgbtdongle => ./lpGBTDongle

require (
	BNL.gov/golpgbtdongle v0.0.0-00010101000000-000000000000
	github.com/wcharczuk/go-chart/v2 v2.1.0
	go-hep.org/x/hep v0.30.1
)

require (
	github.com/c-bata/go-prompt v0.2.6 // indirect
	github.com/go-mmap/mmap v0.6.0 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/gonuts/binary v0.2.0 // indirect
	github.com/hashicorp/go-uuid v1.0.2 // indirect
	github.com/klauspost/compress v1.14.2 // indirect
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mattn/go-tty v0.0.3 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pierrec/lz4/v4 v4.1.8 // indirect
	github.com/pierrec/xxHash v0.1.5 // indirect
	github.com/pkg/term v1.2.0-beta.2 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/sstallion/go-hid v0.0.0-20211019232252-c64377bfa49e // indirect
	github.com/ulikunitz/xz v0.5.10 // indirect
	golang.org/x/exp v0.0.0-20210220032938-85be41e4509f // indirect
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
	golang.org/x/tools v0.1.8 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gonum.org/v1/gonum v0.9.3 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
