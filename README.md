# go lpGBT USB dongle control library and application

## Introduction and dependencies 

This go module is intended for us with the lpGBT Dongle, as provided by CERN. https://gitlab.cern.ch/gbtproj/gbtxprogrammer/-/tree/master/python/python3

The library relies on the hidapi library, available for Linux, Windows and Mac. You should have this library install in the compilation PC, and the go tools , obviously. 

On mac, with homebrew installed : 

```
brew install hidapi
```
On linux (tested on CENTOS7) : 

```
sudo yum install libudev-devel hidapi-devel
```

On Windows, the hidapi library can be installed from [precompiled binaries](https://github.com/libusb/hidapi/releases)


## Compilation

To compile the module : 

```
cd lpGBTDongle
go mod tidy
go build 
cd ..
go mod tidy
go build lpGBTDongleTool.go 
go build lpGBTDongleCLI.go 
./lpGBTDongleTool -h
./lpGBTDongleCLI -h

```

or 

```
source build.sh
```

## Examples

The lpGBTDongleTool is a simple example to use the module. It can read a file and configure the lpGBT via the dongle, with the addresses and filename provided via arguments.

The ALFE2_lpGBT_TestTool is a more complex example targeted for the ALFE2 ASIC. It is meant to test the configuration of the ALFE2 ASIC via lpGBT I2C master, fopr different 40MHz PS clock phases. 

The lpGBTDongleCLI is a rather complete Command Line Interface to communicate to high-level function of the lpGBT via the USB dongle. To use, launch the executable. The available comands are provided via a dropdown menu that you can navigate with keyboard arrows, with help text provided. The commands can be used to interact with low level function of the USB Dongle, with lpGBT function and ALFE2 functions. 


For help with this module, please contact : mbenoit@NOSPAMbnl.gov

