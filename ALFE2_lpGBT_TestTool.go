//go:build linux || darwin
// +build linux darwin

package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	_ "net/http/pprof"
	"os"
	"runtime"
	"runtime/pprof"

	"BNL.gov/golpgbtdongle"
)

func main() {

	logFile, err := os.OpenFile("log.txt", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	if err != nil {
		panic(err)
	}
	mw := io.MultiWriter(os.Stdout, logFile)
	log.SetOutput(mw)

	devaddr := flag.Int("a", 0x0, "I2C base address of the device")
	i2cfreq := flag.Int("fr", 0x3, "I2C frequency, 1=100kHz,2=200kHz,3=400kHz,4=1MHz")
	conffile := flag.String("f", "ALFE2.yaml", "CSV file containing configuration for the clock chip")
	dryrun := flag.Bool("dry", false, "Dry run reading yaml file")
	verbose := flag.Bool("v", false, "print extra verbosity during execution")
	readflag := flag.Bool("r", true, "Readback ALFE2 Configuration")
	ntrials := flag.Int("n", 10, "number of configuration trials per delay")
	clock := flag.Int("c", 1, "PS clock to be used")
	master := flag.Int("m", 0, "lpGBT I2C Master to be used")
	cpuprofile := flag.String("cpuprofile", "", "write cpu profile to `file`")
	memprofile := flag.String("memprofile", "", "write memory profile to `file`")
	flag.Parse()

	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		defer f.Close() // error handling omitted for example
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal("could not start CPU profile: ", err)
		}
		defer pprof.StopCPUProfile()
	}

	if *dryrun {
		log.Println("WARNING!!! This is a dry-run !!")
	}

	log.Printf("Using clock PS%v and I2C Master M%v \n", *clock, *master)

	ctrl := golpgbtdongle.NewALFE2Controller("0", *devaddr, *master, *dryrun, *verbose)
	log.Println("Connecting I2C Level translator...")
	err = ctrl.I2cctrl.GBTxController.Dongle.I2CConnect(true)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Reseting I2C Controller ...")
	ctrl.I2cctrl.GBTxController.Dongle.I2CReset()
	log.Println("Scanning for I2C devices from 0 to 127...")
	slaves, _ := ctrl.I2cctrl.GBTxController.Dongle.I2CScan(0, 127)
	log.Println(slaves)
	log.Println("Ready for operation...")

	ctrl.ReadConfiguration(*conffile)

	ctrl.I2cctrl.GBTxController.GBTx_Dump_Config("lpGBTv1.conf")
	ctrl.SetI2CClockSpeed(uint8(*i2cfreq))
	ctrl.I2cctrl.GBTxController.SetPSClockSpeed(1, 1)
	ctrl.I2cctrl.GBTxController.SetPSClockDrive(7, 1)

	ctrl.SetClockDelay(0, *clock, true)

	//Set reset
	ctrl.I2cctrl.GBTxController.SetGPIOOutputDrive(5, true, true)
	ctrl.I2cctrl.GBTxController.SetGPIODirection(5, true)
	ctrl.ResetALFE2(5)
	ctrl.I2cctrl.GBTxController.ResetI2CMasters()

	csvFile, err := os.Create("ALFE2_I2C_Scan_errors.csv")

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}
	defer csvFile.Close()

	csvwriter := csv.NewWriter(csvFile)
	defer csvwriter.Flush()

	for delay := 0; delay < 512; delay += 4 {
		noack, sdllow, readback := 0, 0, 0

		log.Printf("Setting ALFE2 I2C driver clock delay to %v \n", delay)
		ctrl.SetClockDelay(uint16(delay), *clock, true)
		ctrl.ResetALFE2(5)
		ctrl.I2cctrl.GBTxController.ResetI2CMasters()

		for trials := 0; trials < *ntrials; trials++ {
			ctrl.IncrementConfiguration()
			noackt, sdllowt := ctrl.Configure(*verbose, *dryrun)
			if noackt > 0 || sdllowt > 0 {
				ctrl.ResetALFE2(5)
				ctrl.I2cctrl.GBTxController.ResetI2CMasters()
				noackt, sdllowt = ctrl.Configure(*verbose, *dryrun)
			}
			noack += noackt
			sdllow += sdllowt
			if *readflag {
				noackt, sdllowt, readbackt := ctrl.Readback(*verbose, *dryrun)
				noack += noackt
				sdllow += sdllowt
				readback += readbackt
			}
		}

		log.Printf("%v No Ack incident, %v sdl low incident, %v readback errors during configuration \n", noack, sdllow, readback)
		reccord := []string{fmt.Sprintf("%v", delay), fmt.Sprintf("%v", noack), fmt.Sprintf("%v", sdllow), fmt.Sprintf("%v", readback)}
		csvwriter.Write(reccord)
		csvwriter.Flush()
	}

	if *memprofile != "" {
		f, err := os.Create(*memprofile)
		if err != nil {
			log.Fatal("could not create memory profile: ", err)
		}
		defer f.Close() // error handling omitted for example
		runtime.GC()    // get up-to-date statistics
		if err := pprof.WriteHeapProfile(f); err != nil {
			log.Fatal("could not write memory profile: ", err)
		}
	}
}
