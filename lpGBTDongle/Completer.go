package golpgbtdongle

import (
	"strings"

	prompt "github.com/c-bata/go-prompt"
)

var s []prompt.Suggest = []prompt.Suggest{
	{Text: "load_lpgbt_config", Description: "Load lpGBT configuration file to connected device [filepath]"},
	{Text: "load_alfe2_config", Description: "Load ALFE2 current configuration in memory to connected device [alfe2 i2c address]"},
	{Text: "set_ps_delay", Description: "Set lpGBT PS Clock delay [delay] [PS ID(0,1,2,3)] [fine_tune]"},
	{Text: "set_ps_drive", Description: "Set lpGBT PS Clock drive [drive (0...7)] [PS ID(0,1,2,3)]"},
	{Text: "set_ps_frequency", Description: "Set lpGBT PS Clock frequency [freq (0...6)] [PS ID(0,1,2,3)]"},
	{Text: "set_lpgbt_address", Description: "Set lpGBT I2C address [address]"},
	{Text: "increment_alfe2_config", Description: "increment ALFE2 config in memory"},
	{Text: "set_gpio_direction", Description: "set a gpio pin direction [pin (0-15)] [direction (0 =in, 1=out)]"},
	{Text: "set_gpio_drive", Description: "set a gpio pin drive [pin (0-15)] [drive (0-3] [pullup 0=down 1=up]"},
	{Text: "set_gpio_value", Description: "set a gpio pin value [pin (0-15)] [value=0,1]"},
	{Text: "get_gpio_value", Description: "get a gpio pin value [pin (0-15)]"},
	{Text: "reset_i2c_masters", Description: "reset all lpGBT i2c masters"},
	{Text: "set_config_done", Description: "set dllConfigDone and pllConfigDone to 1"},
	{Text: "set_adc_connection", Description: "set lpGBT internal ADC P and N pin connection [pinP (ADC0-7,VSS,VDD,VDDA,VDDRX,VDDTX,VREF,TEMP,DAC)] [pinN (same)]"},
	{Text: "read_adc", Description: "read samples from lpGBT internal ADC [nsamples] [gain(0-3)]"},
	{Text: "lpgbt_i2c_read", Description: "perform single I2C read on one of lpGBT I2C master [master(0-2)] [address] "},
	{Text: "lpgbt_i2c_write", Description: "perform single I2C write on one of lpGBT I2C master [master(0-2)] [address] [value]"},
	{Text: "lpgbt_reg_read", Description: "perform single lpGBT register read [address]"},
	{Text: "lpgbt_reg_write", Description: "perform single lpGBT register write [address] [value]"},

	{Text: "exit", Description: "quit the CLI"},
}

// Completer provides completion command handler
type Completer struct {
}

// Do provide completion to prompt
func (c *Completer) Do(d prompt.Document) []prompt.Suggest {
	if d.TextBeforeCursor() == "" {
		return s
	}
	args := strings.Split(d.TextBeforeCursor(), " ")

	return c.completeWithArguments(args...)
}

func (c *Completer) completeWithArguments(args ...string) []prompt.Suggest {
	//log.Println(args)

	if len(args) <= 1 {
		return prompt.FilterHasPrefix(s, args[0], true)
	}

	cmd := strings.TrimSpace(args[0])
	//second := args[1]
	if len(args) > 1 {
		switch cmd {
		default:
			return prompt.FilterHasPrefix(s, cmd, true)
		}
	}
	return s
}
