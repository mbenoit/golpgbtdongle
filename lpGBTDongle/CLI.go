package golpgbtdongle

import (
	"log"
	"strconv"
	"strings"

	prompt "github.com/c-bata/go-prompt"
)

func parsehex(val string) (uint16, error) {
	var retvalint uint64
	var err error
	if strings.Contains(val, "0x") || strings.Contains(val, "0X") {
		nval := strings.Replace(val, "0x", "", -1)
		nval = strings.Replace(nval, "0X", "", -1)
		retvalint, err = strconv.ParseUint(nval, 16, 16)
		return uint16(retvalint), err
	} else {
		retvalint, err = strconv.ParseUint(val, 10, 16)
		return uint16(retvalint), err
	}
}

type CommandPrompt struct {
	alfe2ctrl       *ALFE2Controller
	verbose, dryrun bool
}

func (c CommandPrompt) executor(in string) {
	in = strings.TrimSpace(in)
	blocks := strings.Split(in, " ")
	switch blocks[0] {
	case "exit":
		log.Println("Good Bye, see you!")
		return

	case "load_lpgbt_config":
		if len(blocks) < 2 {
			log.Println("Not enough arguments")
			return
		}
		log.Println("load_lpgbt_config ", blocks[1])
		c.alfe2ctrl.I2cctrl.GBTxController.GBTx_Dump_Config(blocks[1])

	case "load_alfe2_config":
		if len(blocks) < 2 {
			log.Println("Not enough arguments")
			return
		}
		log.Println("load_alfe2_config")
		c.alfe2ctrl.Configure(c.verbose, c.dryrun)

	case "set_ps_delay":
		if len(blocks) < 4 {
			log.Println("Not enough arguments")
			return
		}

		delay, err := parsehex(blocks[1])
		clock, err2 := parsehex(blocks[2])
		fine, err3 := parsehex(blocks[3])
		if (err != nil) || (err2 != nil) || (err3 != nil) {
			log.Printf("Could not parse arguments %v %v %v, using %v %v %v", blocks[1], blocks[2], blocks[3], delay, clock, false)
		}
		if fine == 1 {
			c.alfe2ctrl.SetClockDelay(uint16(delay), int(clock), true)

		} else {
			c.alfe2ctrl.SetClockDelay(uint16(delay), int(clock), false)
		}

	case "set_ps_drive":
		if len(blocks) < 3 {
			log.Println("Not enough arguments")
			return
		}
		drive, err := parsehex(blocks[1])
		clock, err2 := parsehex(blocks[2])
		if (err != nil) || (err2 != nil) {
			log.Printf("Could not parse arguments  %v %v, using  %v %v", blocks[1], blocks[2], drive, clock)
		}
		c.alfe2ctrl.I2cctrl.GBTxController.SetPSClockDrive(uint8(drive), int(clock))

	case "set_ps_frequency":
		if len(blocks) < 3 {
			log.Println("Not enough arguments")
			return
		}
		freq, err := parsehex(blocks[1])
		clock, err2 := parsehex(blocks[2])
		if (err != nil) || (err2 != nil) {
			log.Printf("Could not parse arguments  %v %v, using  %v %v", blocks[1], blocks[2], freq, clock)
		}
		c.alfe2ctrl.I2cctrl.GBTxController.SetPSClockSpeed(uint8(freq), int(clock))

	case "set_lpgbt_address":
		if len(blocks) < 2 {
			log.Println("Not enough arguments")
			return
		}
		log.Println("set_lpgbt_address")
		addr, err := parsehex(blocks[1])
		if err != nil {
			log.Printf("Could not parse arguments %v, using %v", blocks[1], addr)
		}
		c.alfe2ctrl.I2cctrl.GBTxController.GBTx_address = int(addr)

	case "increment_alfe2_config":
		log.Println("increment_alfe2_configuration")
		c.alfe2ctrl.IncrementConfiguration()

	case "set_gpio_direction":
		if len(blocks) < 3 {
			log.Println("Not enough arguments")
			return
		}
		log.Println("set_gpio_direction")
		gpio, err := parsehex(blocks[1])
		dir, err2 := parsehex(blocks[2])

		if (err != nil) || (err2 != nil) {
			log.Printf("Could not parse arguments  %v %v, using  %v %v", blocks[1], blocks[2], gpio, dir)
		}
		dirbool := dir != 0
		c.alfe2ctrl.I2cctrl.GBTxController.SetGPIODirection(uint8(gpio), dirbool)

	case "set_gpio_drive":
		if len(blocks) < 4 {
			log.Println("Not enough arguments")
			return
		}
		log.Println("set_gpio_drive")
		gpio, err := parsehex(blocks[1])
		drive, err2 := parsehex(blocks[2])
		pullup, err3 := parsehex(blocks[3])

		if (err != nil) || (err2 != nil) || (err3 != nil) {
			log.Printf("Could not parse arguments  %v %v, using  %v %v", blocks[1], blocks[2], gpio, drive)
		}
		pullbool := pullup != 0
		drivebool := drive != 0
		c.alfe2ctrl.I2cctrl.GBTxController.SetGPIOOutputDrive(uint8(gpio), pullbool, drivebool)

	case "set_gpio_value":
		if len(blocks) < 3 {
			log.Println("Not enough arguments")
			return
		}
		log.Println("set_gpio_value")
		gpio, err := parsehex(blocks[1])
		value, err2 := parsehex(blocks[2])

		if (err != nil) || (err2 != nil) {
			log.Printf("Could not parse arguments  %v %v, using  %v %v", blocks[1], blocks[2], gpio, value)
		}
		valuebool := value != 0
		c.alfe2ctrl.I2cctrl.GBTxController.SetGPIOOutputValue(uint8(gpio), valuebool)

	case "get_gpio_value":
		if len(blocks) < 2 {
			log.Println("Not enough arguments")
			return
		}
		log.Println("get_gpio_value")
		gpio, err := parsehex(blocks[1])

		if err != nil {
			log.Printf("Could not parse arguments  %v, using  %v", blocks[1], gpio)
		}
		value := c.alfe2ctrl.I2cctrl.GBTxController.GetGPIOValue(uint8(gpio))
		log.Printf("GPIO %v has value %v", gpio, value)

	case "reset_i2c_masters":
		log.Println("reset_i2c_masters")
		c.alfe2ctrl.I2cctrl.GBTxController.ResetI2CMasters()

	case "set_config_done":
		log.Println("set_config_done")
		c.alfe2ctrl.I2cctrl.GBTxController.SetlpGBTConfigDone()

	case "set_adc_connection":
		if len(blocks) < 3 {
			log.Println("Not enough arguments")
			return
		}
		log.Println("set_adc_connection")
		c.alfe2ctrl.I2cctrl.GBTxController.SetADCConnection(blocks[1], blocks[2])

	case "lpgbt_i2c_read":
		log.Println("lpgbt_i2c_read")
		if len(blocks) < 3 {
			log.Println("Not enough arguments")
			return
		}
		master, err := parsehex(blocks[1])
		address, err2 := parsehex(blocks[2])
		if (err != nil) || (err2 != nil) {
			log.Printf("Could not parse arguments  %v %v, using  %v %v", blocks[1], blocks[2], master, address)
		}
		c.alfe2ctrl.I2cctrl.master = int(master)
		c.alfe2ctrl.I2cctrl.SetDeviceAddress("test", int(address))
		data, errrd := c.alfe2ctrl.I2cctrl.Read("test", 1)
		if errrd.N_noack > 0 || errrd.N_sdllow > 0 {
			log.Printf("Error when reading 1 byte from address %v on master %v,  error : %v", address, master, errrd)
		} else {
			log.Printf("Read 1 byte from address %v on master %v with value : %v", address, master, data[0])
		}
	case "lpgbt_i2c_write":
		log.Println("lpgbt_i2c_write")
		if len(blocks) < 4 {
			log.Println("Not enough arguments")
			return
		}
		master, err := parsehex(blocks[1])
		address, err2 := parsehex(blocks[2])
		value, err3 := parsehex(blocks[3])
		if (err != nil) || (err2 != nil) || (err3 != nil) {
			log.Printf("Could not parse arguments %v %v %v, using %v %v %v", blocks[1], blocks[2], blocks[3], master, address, value)
		}
		c.alfe2ctrl.I2cctrl.master = int(master)
		c.alfe2ctrl.I2cctrl.SetDeviceAddress("test", int(address))
		errwr := c.alfe2ctrl.I2cctrl.Write("test", []byte{byte(value)})
		if errwr.N_noack > 0 || errwr.N_sdllow > 0 {
			log.Printf("Error when writing 1 byte to address %v on master %v with value : %v, error : %v", address, master, value, errwr)
		} else {
			log.Printf("Wrote 1 byte to address %v on master %v with value : %v", address, master, value)
		}
	case "lpgbt_reg_read":
		log.Println("lpgbt_reg_read")
		if len(blocks) < 2 {
			log.Println("Not enough arguments")
			return
		}
		address, err := parsehex(blocks[1])
		if err != nil {
			log.Printf("Could not parse arguments %v, using %v", blocks[1], address)
		}
		value := c.alfe2ctrl.I2cctrl.GBTxController.GBTx_Read_Register(int(address))
		log.Printf("Register %x, Value : %x", address, value)

	case "lpgbt_reg_write":
		if len(blocks) < 3 {
			log.Println("Not enough arguments")
			return
		}
		address, err := parsehex(blocks[1])
		value, err2 := parsehex(blocks[2])
		log.Printf("lpgbt_reg_write %v %v \n", address, value)

		if (err != nil) || (err2 != nil) {
			log.Printf("Could not parse arguments  %v %v, using  %v %v", blocks[1], blocks[2], address, value)
		}
		c.alfe2ctrl.I2cctrl.GBTxController.GBTx_Write_Register(int(address), int(value))

	case "read_adc":
		log.Println("read_adc")
		if len(blocks) < 3 {
			log.Println("Not enough arguments")
			return
		}
		nsamples, err := parsehex(blocks[1])
		gain, err2 := parsehex(blocks[2])
		if (err != nil) || (err2 != nil) {
			log.Printf("Could not parse arguments  %v %v, using  %v %v", blocks[1], blocks[2], nsamples, gain)
		}
		c.alfe2ctrl.I2cctrl.GBTxController.ReadADC(int(nsamples), uint8(gain))
	default:
		log.Println("command not found")
	}
}

func ExitCheckerCLI(in string, breakline bool) bool {
	if in == "exit" {
		if breakline {
			return true
		} else {
			return false
		}
	} else {
		return false
	}

}

func CLIloop(gbtxaddr, alfe2addr, master int, dryrun, verbose bool) {

	Completer := Completer{}
	CLI := CommandPrompt{alfe2ctrl: NewALFE2Controller("ALFE2_", alfe2addr, master, dryrun, verbose), dryrun: dryrun, verbose: verbose}
	CLI.alfe2ctrl.I2cctrl.GBTxController.GBTx_address = gbtxaddr
	CLI.alfe2ctrl.I2cctrl.AddDevice("generic_device", uint16(gbtxaddr), "lpgbt")

	log.Println("Connecting I2C Level translator...")
	err := CLI.alfe2ctrl.I2cctrl.GBTxController.Dongle.I2CConnect(true)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Reseting I2C Controller ...")
	CLI.alfe2ctrl.I2cctrl.GBTxController.Dongle.I2CReset()
	log.Println("Scanning for I2C devices from 0 to 127...")
	slaves, _ := CLI.alfe2ctrl.I2cctrl.GBTxController.Dongle.I2CScan(0, 127)
	log.Println(slaves)
	log.Println("Ready for operation...")
	p := prompt.New(
		CLI.executor,
		Completer.Do,
		prompt.OptionPrefix("[lpGBTDongle]> "),
		prompt.OptionTitle("http-prompt"),
		prompt.OptionShowCompletionAtStart(),
		prompt.OptionCompletionOnDown(),
		prompt.OptionSetExitCheckerOnInput(ExitCheckerCLI),
	)
	p.Run()
}
