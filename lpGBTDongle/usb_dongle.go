package golpgbtdongle

import (
	"errors"
	"fmt"
	"log"

	//	"time"

	"github.com/sstallion/go-hid"
)

// USB Hardware ID
const IDENT_VENDOR_NUM = 0x16C0
const IDENT_PRODUCT_NUM = 0x05DF
const IDENT_VENDOR_STRING = "cern.ch"
const IDENT_PRODUCT_STRING = "usbi2c"

//USB Misc parameters
const ID = 0x1          // Required by USB protocol
const START_BYTE = 0xDA // Header for LinkM USB command
const REPORT_SIZE = 17  // Not used ?

// Command byte values
const LINKM_CMD_NONE = 0 // no command, do not use
//  I2C commands
const LINKM_CMD_I2CTRANS = 1 // i2c read & write (N args: addr + other)
const LINKM_CMD_I2CWRITE = 2 // i2c write to dev (N args: addr + other)
const LINKM_CMD_I2CREAD = 3  // i2c read         (1 args: addr)
const LINKM_CMD_I2CSCAN = 4  // i2c bus scan     (2 args: start,end)
const LINKM_CMD_I2CCONN = 5  // i2c connect/disc (1 args: 1/0)
const LINKM_CMD_I2CINIT = 6  // i2c init         (0 args: )
//
//  linkm board commands
const LINKM_CMD_VERSIONGET = 100 // return linkm version
const LINKM_CMD_STATLEDSET = 101 // status LED set   (1 args: 1/0)
const LINKM_CMD_STATLEDGET = 102 // status LED get   (0 args)
const LINKM_CMD_PLAYSET = 103    // set params of player state machine
const LINKM_CMD_PLAYGET = 104    // get params of  player state machine
const LINKM_CMD_EESAVE = 105     // save linkm state to EEPROM
const LINKM_CMD_EELOAD = 106     // load linkm state from EEPROM
const LINKM_CMD_GOBOOTLOAD = 107 // trigger USB bootload
//
// custom CERN dongle commands
const GBT_CMD_VTARGETSET = 200 // switch the target LDO on/off (1 args: 1/0)
const GBT_CMD_VTARGETGET = 201 // status V target LDO get   (0 args)
const GBT_CMD_BURNFUSE = 202   // apply Efuse voltage and pulse (0 args)
const GBT_CMD_IO1SET = 203     // switch the IO1 output on/off (1 args: 1/0)
const GBT_CMD_IO1GET = 204     // IO1 output status get (0 args)
const GBT_CMD_IO2SET = 205     // switch the IO2 output on/off (1 args: 1/0)
const GBT_CMD_IO2GET = 206     // IO2 output status get (0 args)
const GBT_CMD_VFUSESET = 207   // switch the 3.3V efuse LDO on/off (1 args: 1/0)
const GBT_CMD_VFUSEGET = 208   // get the 3.3V efuse LDO status (0 args)
const GBT_CMD_FUSEPULSE = 209  // trigger a 1 ms pulse width (0 args)
//
// Return codes
const LINKM_ERR_NONE = 0 //No error
const LINKM_ERR_BADSTART = 101
const LINKM_ERR_BADARGS = 102 // command could not be decoded properly
const LINKM_ERR_I2C = 103
const LINKM_ERR_I2CREAD = 104
const LINKM_ERR_NOTOPEN = 199
const LINKM_ERR_USBOVERFLOW = 200 // dongle usb return buffer overflow

func HandleError(code byte) string {
	switch code {
	case LINKM_ERR_BADSTART:
		return "[Dongle Error] Bad Start code error"
	case LINKM_ERR_BADARGS:
		return "[Dongle Error] Bad arguments for command"
	case LINKM_ERR_I2C:
		return "[Dongle Error] I2C Communication error, is the device address correct?"
	case LINKM_ERR_I2CREAD:
		return "[Dongle Error] I2C Read error"
	case LINKM_ERR_NOTOPEN:
		return "[Dongle Error] Connection not open"
	case LINKM_ERR_USBOVERFLOW:
		return "[Dongle Error] USB Bus overflow"
	default:
		return ""
	}
}

type usbdongle struct {
	dev                *hid.Device
	emulation, verbose bool
}

type USBError struct {
	ErrorID int
}

func (err USBError) Error() string {
	switch err.ErrorID {
	case LINKM_ERR_NONE:
		return "No errors \n "
	case LINKM_ERR_BADSTART:
		return "Bad Start Error \n"
	case LINKM_ERR_BADARGS:
		return "USB Command could not be decoded properly \n"
	case LINKM_ERR_I2C:
		return "I2C Error \n"
	case LINKM_ERR_I2CREAD:
		return "I2C Read error \n"
	case LINKM_ERR_NOTOPEN:
		return "I2C Controller not open \n"
	case LINKM_ERR_USBOVERFLOW:
		return "USB Dongle buffer overflow \n"
	default:
		return ""
	}
}

func (usb *usbdongle) Init(emulation, verbose bool) error {
	// Initialize the hid package.

	usb.emulation = emulation
	usb.verbose = verbose
	usb.dev = new(hid.Device)

	var err error

	if !emulation {
		if err = hid.Init(); err != nil {
			return err
		}
		// Open the device using the VID and PID.
		if usb.verbose {
			usb.ShowHIDs()
		}
		usb.dev, err = hid.OpenFirst(IDENT_VENDOR_NUM, IDENT_PRODUCT_NUM)

		if err != nil {
			return err
		}
		mnfct, _ := usb.dev.GetMfrStr()
		prdt, _ := usb.dev.GetProductStr()

		fwversion, _ := usb.GetFirmwareVersion()
		log.Println("")
		log.Printf("lpGBT Dongle Device found, Firmware Version : %v, Manufacturer : %v, product : %v", fwversion, mnfct, prdt)

	} else {
		log.Println("[USBDongle] Running in Emulation mode")
	}
	return nil
}

func (usb *usbdongle) Close() error {
	if !usb.emulation {
		return usb.dev.Close()
	} else {
		return nil
	}
}

func (usb *usbdongle) ShowHIDs() error {
	if !usb.emulation {
		return hid.Enumerate(hid.VendorIDAny, hid.ProductIDAny,
			func(info *hid.DeviceInfo) error {
				fmt.Printf("%s: ID %04x:%04x %s %s\n",
					info.Path,
					info.VendorID,
					info.ProductID,
					info.MfrStr,
					info.ProductStr)
				return nil
			})
	} else {
		log.Println("Emulation mode")
		return nil
	}
}

func (usb *usbdongle) usb_command(cmd, num_send, num_recv int, payload []byte) (readback []byte, err error) {

	buf := make([]byte, 0, 132)
	buf = append(buf, ID, START_BYTE, byte(cmd), byte(num_send), byte(num_recv))
	buf = append(buf, payload...)

	buf = buf[0:132] // make buffer 132 bytes
	var n int
	if !usb.emulation {
		if num_send >= 0 {
			n, err = usb.dev.SendFeatureReport(buf)
			if usb.verbose {
				log.Println("Writing via USB", n, buf[:num_send+5])
			}
			if err != nil {
				return nil, err
			}
		}
		if num_recv > 0 {
			readback = make([]byte, 17)
			readback[0] = ID
			n, err = usb.dev.GetFeatureReport(readback)
			if err != nil {
				log.Println("Warning, USB Read timeout", err)
			}
			if usb.verbose {
				log.Println("Reading via USB", n, readback)
			}

			if readback[1] == LINKM_ERR_NONE {
				readback = readback[1 : num_recv+2]
				return
			} else {
				err = errors.New(HandleError(readback[1]))
				return
			}
		} else {
			return
		}

	} else {
		log.Printf("Writing %v to device, cmd = %v, sent byte = %v, reading back %v bytes \n", buf[:num_send+4], cmd, num_send, num_recv)
		for i := 0; i <= num_recv; i++ {
			readback = append(readback, byte(i))
		}
		return
	}
}

func (usb *usbdongle) SetVtargetLDO(value bool) error {
	log.Println("Setting Target LDO Voltage to ", value)
	var err error
	if value {
		_, err = usb.usb_command(GBT_CMD_VTARGETSET, 1, 0, []byte{byte(1)})
	} else {
		_, err = usb.usb_command(GBT_CMD_VTARGETSET, 1, 0, []byte{byte(0)})
	}
	return err
}

func (usb *usbdongle) SetOD1(value bool) error {
	state := 0
	if value {
		state = 1
	}
	_, err := usb.usb_command(GBT_CMD_IO1SET, 1, 0, []byte{byte(state)})
	return err
}

func (usb *usbdongle) SetOD2(value bool) error {
	state := 0
	if value {
		state = 1
	}
	_, err := usb.usb_command(GBT_CMD_IO2SET, 1, 0, []byte{byte(state)})
	return err
}

func (usb *usbdongle) GetOD1() (int, error) {
	data, err := usb.usb_command(GBT_CMD_IO1GET, 0, 1, []byte{})
	return int(data[0]), err
}

func (usb *usbdongle) GetOD2() (int, error) {
	data, err := usb.usb_command(GBT_CMD_IO2GET, 0, 1, []byte{})
	return int(data[0]), err
}

func (usb *usbdongle) BurnFuse() error {
	_, err := usb.usb_command(GBT_CMD_BURNFUSE, 1, 0, []byte{})
	return err
}

func (usb *usbdongle) GetFirmwareVersion() (string, error) {
	data, err := usb.usb_command(LINKM_CMD_VERSIONGET, 1, 3, []byte{})
	return fmt.Sprintf("%v.%v", rune(data[0]), rune(data[1])), err
}

func (usb *usbdongle) GoBootload() error {
	_, err := usb.usb_command(LINKM_CMD_GOBOOTLOAD, 0, 0, []byte{})
	return err
}

func (usb *usbdongle) SetVfuseLDO(value int) error {
	_, err := usb.usb_command(GBT_CMD_VFUSESET, 1, 0, []byte{byte(value)})
	return err
}

func (usb *usbdongle) FusePulse() error {
	_, err := usb.usb_command(GBT_CMD_FUSEPULSE, 0, 0, []byte{})
	return err
}

func (usb *usbdongle) I2CReset() error {
	_, err := usb.usb_command(LINKM_CMD_I2CINIT, 0, 0, []byte{})
	return err
}

func (usb *usbdongle) I2CConnect(value bool) error {
	state := 0
	if value {
		state = 1
	}
	_, err := usb.usb_command(LINKM_CMD_I2CCONN, 1, 0, []byte{byte(state)})
	return err
}

func (usb *usbdongle) I2CScan(start, end int) ([]byte, error) {
	data, err := usb.usb_command(LINKM_CMD_I2CSCAN, 2, 14, []byte{byte(start), byte(end)})
	log.Printf("Found %v slaves, %v \n", int(data[1]), data)
	return data[2:(2 + data[1])], err
}

func (usb *usbdongle) I2CWrite(addr int, data []byte) error {
	payload := []byte{byte(addr)}
	payload = append(payload, data...)
	_, err := usb.usb_command(LINKM_CMD_I2CWRITE, len(payload), 0, payload)
	return err
}

func (usb *usbdongle) I2CRead(addr, n_to_read int) (readdata []byte, err error) {
	readdata, err = usb.usb_command(LINKM_CMD_I2CREAD, 1, n_to_read, nil)
	return
}

func (usb *usbdongle) I2CWriteRead(addr, n_to_read int, data []byte) (readdata []byte, err error) {
	payload := []byte{byte(addr)}
	payload = append(payload, data...)
	readdata, err = usb.usb_command(LINKM_CMD_I2CTRANS, len(payload), n_to_read, payload)
	return
}
