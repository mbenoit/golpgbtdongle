package golpgbtdongle

import (
	"bufio"
	"log"
	"os"
	"strings"
	"time"
)

const PS0_CONFIG = 0x05d
const PS0_DELAY = 0x05e
const PS1_CONFIG = 0x060
const PS1_DELAY = 0x061
const PS2_CONFIG = 0x063
const PS2_DELAY = 0x064
const PS3_CONFIG = 0x066
const PS3_DELAY = 0x067

const PIODIRH = 0x053
const PIODIRL = 0x054
const PIOOUTH = 0x055
const PIOOUTL = 0x056
const PIOPULLENAH = 0x57
const PIOPULLENAL = 0x58
const PIOUPDOWNH = 0x59
const PIOUPDOWNL = 0x5a
const PIODRIVESTRENGTHH = 0x5b
const PIODRIVESTRENGTHL = 0x5c
const PIOINH = 0x1af
const PIOINL = 0x1b0

const RST0 = 0x13c
const POWERUP2 = 0x0fb
const ADCSELECT = 0x121
const ADCCONFIG = 0x123
const ADCSTATUSH = 0x1ca
const ADCSTATUSL = 0x1cb

const LPGBT_MAX_RW_REG = 0x14c

func setlpGBTPSClockRegister(clock int) (int, int) {
	switch clock {
	case 0:
		return PS0_CONFIG, PS0_DELAY
	case 1:
		return PS1_CONFIG, PS1_DELAY
	case 2:
		return PS2_CONFIG, PS2_DELAY
	case 3:
		return PS3_CONFIG, PS3_DELAY
	default:
		return PS0_CONFIG, PS0_DELAY
	}

}

type GBTx struct {
	Dongle             *usbdongle
	GBTx_address       int
	emulation, verbose bool
	name_map           map[string]*lpGBTRegister
	addr_map           map[int]string
}

func (gbt *GBTx) Init(emulation, verbose bool) (err error) {
	gbt.GBTx_address = 127
	gbt.Dongle = &usbdongle{}
	gbt.emulation = emulation
	gbt.verbose = verbose
	err = gbt.Dongle.Init(emulation, verbose)
	if err != nil {
		return err
	}
	err = gbt.Dongle.SetVtargetLDO(true)

	if err != nil {
		return err
	}
	err = gbt.Dongle.I2CConnect(true)

	gbt.name_map, gbt.addr_map = GetlpGBTRegisterMap()

	return
}

func (gbt *GBTx) GBTx_Write_Register(register, value int) {
	if register > LPGBT_MAX_RW_REG {
		log.Printf("Register address %x is read only", register)
		return
	}
	if name, ok := gbt.addr_map[register]; ok {
		gbt.name_map[name].Value = uint8(value)
		if gbt.verbose {
			log.Printf("Writing %x to register %v, addr = %x", gbt.name_map[name].Value, name, gbt.name_map[name].Address)
		}
	}
	reg_add_l := register & 0xFF
	reg_add_h := (register >> 8) & 0xFF
	payload := []byte{byte(reg_add_l), byte(reg_add_h), byte(value)}
	err := gbt.Dongle.I2CWrite(gbt.GBTx_address, payload)

	if err != nil {
		log.Println("Error : ", err)
	}
}
func (gbt *GBTx) GBTx_Read_Register(register int) byte {
	if name, ok := gbt.addr_map[register]; ok {
		if gbt.verbose {
			log.Printf("Reading register %v, addr = %x", name, gbt.name_map[name].Address)
		}
	}
	reg_add_l := register & 0xFF
	reg_add_h := (register >> 8) & 0xFF
	payload := []byte{byte(reg_add_l), byte(reg_add_h)}
	value, err := gbt.Dongle.I2CWriteRead(gbt.GBTx_address, 1, payload)
	if err != nil {
		log.Println("Error : ", err)
	}
	return value[1]
}

func (gbt *GBTx) GBTx_Read_BlockRegister(register int) []byte {
	if register+15 > LPGBT_MAX_RW_REG {
		log.Printf("WARINNG : Address range %x to %x cross tthe read-only part of the register map", register, register+15)
	}
	reg_add_l := register & 0xFF
	reg_add_h := (register >> 8) & 0xFF
	payload := []byte{byte(reg_add_l), byte(reg_add_h)}
	value, err := gbt.Dongle.I2CWriteRead(gbt.GBTx_address, 15, payload)
	if err != nil {
		log.Println("Error : ", err)
	}
	return value[1:]
}

func (gbt *GBTx) GBTx_Dump_Config(filename string) (err error) {

	f, err := os.Open(filename)
	if err != nil {
		log.Println("Error : ", err)
		return err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	reg := 0
	for scanner.Scan() {
		line := scanner.Text()
		words := strings.Split(line, " ")

		name := words[0]
		address, err := parsehex(words[1])
		value, err := parsehex(words[2])

		if err != nil {
			log.Println("Error : ", err, ", aborting ...")
			return err
		}
		log.Printf("Configuring %v with address %v and value %v", name, address, value)
		gbt.GBTx_Write_Register(int(address), int(value))
		//time.Sleep(500 * time.Millisecond)
		reg++
	}
	log.Println("Configuration done")
	return
}
func (gbt *GBTx) GBTxReset() {
	err := gbt.Dongle.SetOD1(true)
	if err != nil {
		log.Println("Error : ", err)
	}
	time.Sleep(500 * time.Millisecond)
	err = gbt.Dongle.SetOD1(false)
	if err != nil {
		log.Println("Error : ", err)
	}
	time.Sleep(500 * time.Millisecond)
}

func (gbt *GBTx) VTRxReset() {
	err := gbt.Dongle.SetOD2(true)
	if err != nil {
		log.Println("Error : ", err)
	}
	time.Sleep(500 * time.Millisecond)
	err = gbt.Dongle.SetOD2(false)
	if err != nil {
		log.Println("Error : ", err)
	}
	time.Sleep(500 * time.Millisecond)
}

func (gbt *GBTx) GetGBTxIdle() bool {
	idle := ((gbt.GBTx_Read_Register(431) >> 2) % 0x1F) == 0b11000
	return idle
}

func (gbt *GBTx) SetPSClockDelay(delay uint16, clock int, fine_tuning bool) {
	PS_CONFIG, PS_DELAY := setlpGBTPSClockRegister(clock)
	current := gbt.GBTx_Read_Register(PS_CONFIG)
	freq := current & 0b111
	drive := (current >> 3) & 0b111
	fine := (current >> 6) & 0b1

	var delay_msb, delay_lsb uint16
	if fine_tuning {
		fine = 1
		delay = delay & 0b111111111
		delay_msb = (delay >> 8) & 0b1
		delay_lsb = delay & 0xFF

	} else {
		fine = 0
		delay_msb = (delay >> 4) & 0b1
		delay_lsb = (delay & 0b1111) << 4
	}

	value := freq + (fine << 6) + (drive << 3) + (byte(delay_msb) << 7)
	gbt.GBTx_Write_Register(PS_CONFIG, int(value))
	value = byte(delay_lsb)
	gbt.GBTx_Write_Register(PS_DELAY, int(value))
}

func (gbt *GBTx) SetPSClockSpeed(speed uint8, clock int) {

	speeds := []int{0, 40, 80, 160, 320, 640, 1200}
	log.Printf("Setting clock %v to %v MHz", clock, speeds[clock])
	PS_CONFIG, _ := setlpGBTPSClockRegister(clock)
	current := gbt.GBTx_Read_Register(PS_CONFIG)
	freq := speed & 0b111
	drive := (current >> 3) & 0b111
	fine := (current >> 6) & 0b1
	delay_msb := (current >> 7) & 0b1

	if speed > 6 {
		speed = 6
		log.Println("Speed too high, using 6 (1200 MHz)")
	}

	value := freq + (fine << 6) + (delay_msb << 7) + (drive << 3)
	gbt.GBTx_Write_Register(PS_CONFIG, int(value))
}

func (gbt *GBTx) SetPSClockDrive(drive_strength uint8, clock int) {
	PS_CONFIG, _ := setlpGBTPSClockRegister(clock)
	current := gbt.GBTx_Read_Register(PS_CONFIG)
	freq := current & 0b111
	drive := drive_strength & 0b111
	fine := (current >> 6) & 0b1
	delay_msb := (current >> 7) & 0b1

	if drive_strength > 7 {
		log.Println("drive strength too high, using 7 (4 mA)")
	}

	value := freq + (fine << 6) + (delay_msb << 7) + (drive << 3)
	gbt.GBTx_Write_Register(PS_CONFIG, int(value))
}

func (gbt *GBTx) SetGPIODirection(gpio uint8, direction bool) {
	var PIODIR uint8
	var subpio uint8

	if (gpio < 16) && (gpio > 7) {
		PIODIR = PIODIRH
		subpio = gpio - 8
	} else if gpio < 8 {
		PIODIR = PIODIRL
		subpio = gpio
	} else {
		log.Println("Select existing GPIO pin")
		return
	}
	current_piodir := gbt.GBTx_Read_Register(int(PIODIR))
	if direction {
		current_piodir |= (1 << subpio)
	} else {
		current_piodir &^= (1 << subpio)
	}
	gbt.GBTx_Write_Register(int(PIODIR), int(current_piodir))
}

func (gbt *GBTx) SetGPIOOutputValue(gpio uint8, value bool) {
	var PIOOUT uint8
	var subpio uint8

	if (gpio < 16) && (gpio > 7) {
		PIOOUT = PIOOUTH
		subpio = gpio - 8
	} else if gpio < 8 {
		PIOOUT = PIOOUTL
		subpio = gpio
	} else {
		log.Println("Select existing GPIO pin")
		return
	}
	current_piodir := gbt.GBTx_Read_Register(int(PIOOUT))
	if value {
		current_piodir |= (1 << subpio)
	} else {
		current_piodir &^= (1 << subpio)
	}
	gbt.GBTx_Write_Register(int(PIOOUT), int(current_piodir))
}

func (gbt *GBTx) GetGPIOValue(gpio uint8) bool {
	var PIOIN uint8
	var subpio uint8

	if (gpio < 16) && (gpio > 7) {
		PIOIN = PIOOUTH
		subpio = gpio - 8
	} else if gpio < 8 {
		PIOIN = PIOOUTL
		subpio = gpio
	} else {
		log.Println("Select existing GPIO pin")
		return false
	}
	current_pio := gbt.GBTx_Read_Register(int(PIOIN))

	value := ((current_pio << subpio) & 0b1)

	if value == 1 {
		return true
	} else {
		return false
	}

}

func (gbt *GBTx) SetGPIOOutputDrive(gpio uint8, pullup, drive bool) {
	var PIOUPDOWN uint8
	var PIOPULLENA uint8
	var PIODRIVESTRENGTH uint8

	var subpio uint8

	if (gpio < 16) && (gpio > 7) {
		PIOUPDOWN = PIOUPDOWNH
		PIOPULLENA = PIOPULLENAH
		PIODRIVESTRENGTH = PIODRIVESTRENGTHH
		subpio = gpio - 8
	} else if gpio < 8 {
		PIOUPDOWN = PIOUPDOWNL
		PIOPULLENA = PIOPULLENAL
		PIODRIVESTRENGTH = PIODRIVESTRENGTHL
		subpio = gpio
	} else {
		log.Println("Select existing GPIO pin")
		return
	}
	current_pioupdown := gbt.GBTx_Read_Register(int(PIOUPDOWN))
	current_piopullena := gbt.GBTx_Read_Register(int(PIOPULLENA))
	current_piodrive := gbt.GBTx_Read_Register(int(PIODRIVESTRENGTH))

	if pullup {
		current_pioupdown |= (1 << subpio)
	} else {
		current_pioupdown &^= (1 << subpio)
	}
	current_piopullena |= (1 << subpio)

	if drive {
		current_piodrive |= (1 << subpio)
	} else {
		current_piodrive &^= (1 << subpio)
	}

	gbt.GBTx_Write_Register(int(PIOUPDOWN), int(current_pioupdown))
	gbt.GBTx_Write_Register(int(PIODRIVESTRENGTH), int(current_piodrive))
	gbt.GBTx_Write_Register(int(PIOPULLENA), int(current_piopullena))
}

func (gbt *GBTx) ResetI2CMasters() {
	log.Println("Resting lpGBT I2C Masters")
	gbt.GBTx_Write_Register(RST0, 0x0)
	gbt.GBTx_Write_Register(RST0, 0b111)
	gbt.GBTx_Write_Register(RST0, 0x0)
	time.Sleep(100 * time.Millisecond)
}

func (gbt *GBTx) SetlpGBTConfigDone() {
	log.Println("Setting pllConfigDone and dllConfigDone to 1")
	gbt.GBTx_Write_Register(POWERUP2, 0b110)
}

func (gbt *GBTx) SetADCConnection(pinP, pinN string) {
	var low, high int
	switch pinP {
	case "ADC0":
		high = 0
	case "ADC1":
		high = 1
	case "ADC2":
		high = 2
	case "ADC3":
		high = 3
	case "ADC4":
		high = 4
	case "ADC5":
		high = 5
	case "ADC6":
		high = 6
	case "ADC7":
		high = 7
	case "DAC":
		high = 8
	case "VSSA":
		high = 9
	case "VDDTX":
		high = 10
	case "VDDRX":
		high = 11
	case "VDD":
		high = 12
	case "VDDA":
		high = 13
	case "TEMP":
		high = 14
	case "VREF":
		high = 15
	default:
		log.Fatal("Unknown source for ADC")
	}

	switch pinN {
	case "ADC0":
		low = 0
	case "ADC1":
		low = 1
	case "ADC2":
		low = 2
	case "ADC3":
		low = 3
	case "ADC4":
		low = 4
	case "ADC5":
		low = 5
	case "ADC6":
		low = 6
	case "ADC7":
		low = 7
	case "DAC":
		low = 8
	case "VSSA":
		low = 9
	case "VDDTX":
		low = 10
	case "VDDRX":
		low = 11
	case "VDD":
		low = 12
	case "VDDA":
		low = 13
	case "TEMP":
		low = 14
	case "VREF":
		low = 15
	default:
		log.Println("Unknown source for ADC")
		return
	}

	gbt.GBTx_Write_Register(ADCSELECT, (low&0b1111)+((high<<4)&0b1111))

}

func (gbt *GBTx) ReadADC(nsamples int, gain uint8) (samples []int) {

	if gain > 3 {
		log.Println("Gain too high, select from 0-3")
		return
	}
	//Start conversion
	gbt.GBTx_Write_Register(ADCCONFIG, int((1<<7)+(gain&0b11)+(1<<2)))

	samples = make([]int, nsamples)

	for n := 0; n < nsamples; n++ {
		high := gbt.GBTx_Read_Register(ADCSTATUSH)
		status := (high >> 6) & 0b1
		trials := 0

		for (status != 1) && (trials < 10) {
			high := gbt.GBTx_Read_Register(ADCSTATUSH)
			status = (high >> 6) & 0b1
			trials++
		}

		if (trials == 10) && (status == 0) {
			log.Println("Failed to perform ADC readout after ten trials")
			return
		}

		low := gbt.GBTx_Read_Register(ADCSTATUSL)
		value := ((int(high) & 0b11) << 8) + int(low)
		samples[n] = value
		log.Printf("ADC Readout of sample %v : %x \n", n, value)
	}

	return
}
