package golpgbtdongle

import (
	"fmt"
	"io/ioutil"
	"log"
	"sort"
	"strconv"
	"time"

	yaml "gopkg.in/yaml.v3"
)

func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}

func bitfield(n, l uint64) []int {
	//n in integer to convert to bitlist, l is the fixed length of the list
	format := fmt.Sprintf("%%0%vb", l)
	b := fmt.Sprintf(format, n)
	result := make([]int, l)
	for i, val := range b {
		result[i], _ = strconv.Atoi(string(val))
	}
	return result
}

func bitlisttoInt(bitlist []int) int64 {
	// transform a bitlist to a int
	out := int64(0)
	//fmt.Println("bitlist ", bitlist)
	for _, bit := range bitlist {
		out = (out << 1) | int64(bit)
	}
	return out & 0xFF
}

func reverse(numbers []int) []int {
	n := len(numbers)
	newnumbers := make([]int, n)
	for i, number := range numbers {
		newnumbers[n-1-i] = number
	}
	return newnumbers
}

type alfe2controller interface {
	SetRegister(name string, value uint32)
	//GetRegister(name string) uint32
	Configure()
	ReadConfiguration() []map[string][]int
}

type ALFE2Controller struct {
	alfe2controller
	I2cctrl                *I2CController
	alfve2_registers_50Ohm map[string]*RegDescription
	alfve2_registers_25Ohm map[string]*RegDescription
	alfve2_registers       map[string]*RegDescription
	i2cmap                 []map[string][]int
	Addr                   int
	DevName                string
	verbose                bool
}

func NewALFE2Controller(name string, addr, master int, dryrun, verbose bool) *ALFE2Controller {

	i2cctrl := I2CController{}
	i2cctrl.Init(dryrun, verbose, master)

	if !dryrun {
		i2cctrl.AddDevice(name+"R0", uint16(addr), "I2CDongle")
		i2cctrl.AddDevice(name+"R1", uint16(addr+0x1), "I2CDongle")
		i2cctrl.AddDevice(name+"R2", uint16(addr+0x2), "I2CDongle")
	}
	i2cmap, reg, reg50, reg25 := GetALFE2RegisterMaps()
	ctrl := ALFE2Controller{DevName: name, Addr: addr, I2cctrl: &i2cctrl, i2cmap: i2cmap,
		alfve2_registers: reg, alfve2_registers_50Ohm: reg50, alfve2_registers_25Ohm: reg25, verbose: verbose}
	return &ctrl
}

func (c *ALFE2Controller) SetRegister(regname string, value uint32) {
	(c.alfve2_registers[regname]).SetValue(uint64(value))
	c.Configure(false, false)
}

func (c *ALFE2Controller) SetClockDelay(delay uint16, clock int, fine bool) {
	c.I2cctrl.GBTxController.SetPSClockDelay(delay, clock, fine)
}

func (c *ALFE2Controller) ResetALFE2(gpio int) {
	c.I2cctrl.GBTxController.SetGPIOOutputValue(uint8(gpio), true)
	time.Sleep(100 * time.Microsecond)
	c.I2cctrl.GBTxController.SetGPIOOutputValue(uint8(gpio), false)
	time.Sleep(100 * time.Microsecond)
	c.I2cctrl.GBTxController.SetGPIOOutputValue(uint8(gpio), true)

}

func (c *ALFE2Controller) SetI2CClockSpeed(value uint8) {

	_, _, I2CDATA0, I2CCMD, _, _ := SetlpGBTMasterAddress(c.I2cctrl.master)

	if value >= 4 {
		log.Printf("Cannot set this I2C bus speed, reverting to 1MHz \n")

		c.I2cctrl.GBTxController.GBTx_Write_Register(I2CDATA0, int(4))
		c.I2cctrl.GBTxController.GBTx_Write_Register(I2CCMD, I2C_WRITE_CR)
	} else {
		switch value {
		case 0:
			log.Println("Configuring I2C bust speed to 100kHz")
		case 1:
			log.Println("Configuring I2C bust speed to 200kHz")
		case 2:
			log.Println("Configuring I2C bust speed to 400kHz")
		case 3:
			log.Println("Configuring I2C bust speed to 1MHz")
		default:
			log.Println("Configuring I2C bust speed to 100kHz")
		}
		c.I2cctrl.GBTxController.GBTx_Write_Register(I2CDATA0, int(value))
		c.I2cctrl.GBTxController.GBTx_Write_Register(I2CCMD, I2C_WRITE_CR)
	}
}

func (c *ALFE2Controller) IncrementConfiguration() {
	safedacs := []string{"PA_OUT_OFFSET", "IMPED_COARSE", "IMPED_FINE", "PA_OUTPUT_DC", "SH_PT", "EN_SH_LG_GAIN_BOOST", "SH_OUTPUT_DC", "SH_SUM_GAIN_CH0", "SH_SUM_GAIN_CH1", "SH_SUM_GAIN_CH2", "SH_SUM_GAIN_CH3", "SH_SUM_OUTPUT", "UNUSED"}
	for key := range c.alfve2_registers {
		if contains(safedacs, key) {
			if c.alfve2_registers[key].Value == ((1 << c.alfve2_registers[key].Length) - 1) {
				c.alfve2_registers[key].Value = 0
			} else {
				c.alfve2_registers[key].Value = ((c.alfve2_registers[key].Value << 1) & 0xFFFFFFE) + 1
			}
		}
		log.Printf("incrementing %v to %v", key, c.alfve2_registers[key].Value)
	}
}
func (c *ALFE2Controller) Configure(verbose, dryrun bool) (noack, sdllow int) {

	transactions := GenerateI2CRegisters(c.alfve2_registers)

	for _, t := range transactions {
		if verbose {
			log.Printf("Writing %x to register with address %x \n", t[1], t[0])
		}
		if !dryrun {
			errors := make([]I2CError, 3)
			errors[0] = c.I2cctrl.Write(c.DevName+"R0", []byte{byte(t[0] & 0xFF)})
			errors[1] = c.I2cctrl.Write(c.DevName+"R1", []byte{byte(0)})
			errors[2] = c.I2cctrl.Write(c.DevName+"R2", []byte{byte(t[1])})
			for _, err := range errors {
				noack += err.N_noack
				sdllow += err.N_sdllow
			}
		}
	}
	return
}

func (c *ALFE2Controller) Readback(verbose, dryrun bool) (noack, sdllow, readback int) {

	transactions := GenerateI2CRegisters(c.alfve2_registers)

	for _, t := range transactions {
		if !dryrun {
			errors := make([]I2CError, 3)
			var value []byte
			errors[0] = c.I2cctrl.Write(c.DevName+"R0", []byte{byte(t[0] & 0xFF)})
			errors[1] = c.I2cctrl.Write(c.DevName+"R1", []byte{byte(0)})
			value, errors[2] = c.I2cctrl.Read(c.DevName+"R2", 1)

			// if (err_r1 != nil) || (err_r2 != nil) || (err_r3 != nil) {
			// 	log.Fatal(err_r1, err_r2, err_r3)
			// }
			for _, err := range errors {
				noack += err.N_noack
				sdllow += err.N_sdllow
			}

			if value[0] != byte(t[1]) {
				readback += 1
			}

			log.Printf("Reading %x from register with address %x, expected %x \n", value, t[0], t[1])

		}
	}
	return
}

func (ctrl *ALFE2Controller) WriteConfiguration(filename string) {
	data, err := yaml.Marshal(&ctrl.alfve2_registers)
	if err != nil {
		log.Fatal(err)
	}

	err2 := ioutil.WriteFile(filename, data, 777)

	if err2 != nil {

		log.Fatal(err2)
	}

	fmt.Println("ALFE2 Registers written to ", filename)
	for key, reg := range ctrl.alfve2_registers {
		log.Printf("%v = %x \n", key, (*reg).Value)
	}
}

func (ctrl *ALFE2Controller) ReadConfiguration(filename string) {
	log.Println("Reading configuration from file ", filename)
	yfile, err := ioutil.ReadFile(filename)
	if err != nil {

		log.Fatal(err)
	}

	err2 := yaml.Unmarshal(yfile, &ctrl.alfve2_registers)

	if err2 != nil {
		log.Fatal(err2)
	}
	log.Printf("ALFE2 Registers read from %s\n", filename)
	for key, reg := range ctrl.alfve2_registers {
		log.Printf("%v = %x \n", key, (*reg).Value)
	}
}

type RegDescription struct {
	Length uint64
	Value  uint64
}

func (reg *RegDescription) SetValue(value uint64) {
	reg.Value = value
}

func GetALFE2RegisterMaps() (alfe2_i2c_map []map[string][]int, alfve2_registers_50Ohm, alfve2_registers_25Ohm, alfve2_registers map[string]*RegDescription) {

	alfe2_i2c_map = []map[string][]int{
		0x0: {"CH_PWR": {0, 4}, "PA_FB_R0": {0, 2}, "PA_FB_C0": {0, 2}},
		0x1: {"PA_OUT_OFFSET": {0, 4}, "PA_FB_C1": {0, 2}, "IMPED_COARSE": {0, 2}},
		0x2: {"IMPED_FINE": {0, 8}},
		0x3: {"IMPED_FINE": {8, 16}},
		0x4: {"IMPED_FINE": {16, 21}, "PA_FB_R1": {0, 1}, "PA_OUTPUT_DC": {0, 2}},
		0x5: {"PA_OUTPUT_DC": {2, 8}, "SH_PT": {0, 2}},
		0x6: {"SH_PT": {2, 10}},
		0x7: {"ENABLE_50_SH": {0, 1}, "SH_OUTPUT_DC": {0, 7}},
		0x8: {"SH_OUTPUT_DC": {7, 15}},
		0x9: {"SH_SUM_PWR": {0, 1}, "SH_SUM_GAIN_CH0": {0, 3}, "SH_SUM_GAIN_CH1": {0, 3}, "SH_SUM_GAIN_CH2": {0, 1}},
		0xA: {"SH_SUM_GAIN_CH2": {1, 3}, "SH_SUM_GAIN_CH3": {0, 3}, "SH_SUM_OUTPUT": {0, 3}},
		0xB: {"SH_SUM_OUTPUT": {3, 11}},
		0xC: {"SH_SUM_OUTPUT": {11, 15}, "DACN0_CUR": {2, 6}},
		0xD: {"DACN0_CUR": {0, 2}, "DACN1_CUR": {0, 6}},
		0xE: {"DACP1_CUR": {0, 6}, "BGR_SEL": {0, 2}},
		0xF: {"PA12V_CUR": {0, 1}, "SH1V2_CUR": {0, 1}, "PA2V5_CUR": {0, 1}, "UNUSED": {0, 4}},
	}

	alfve2_registers_50Ohm = map[string]*RegDescription{
		"CH_PWR":          {4, 0b1111},                   // migh be 1111    // Channel power
		"PA_FB_R0":        {2, 0b11},                     // Feedback Resistor 0
		"PA_FB_C0":        {2, 0b11},                     // feedback capacitor 0
		"PA_OUT_OFFSET":   {4, 0b0001},                   // Preamp Output Offset
		"PA_FB_C1":        {2, 0b01},                     // Preamp Feedback Capacitor 1
		"IMPED_COARSE":    {2, 0b11},                     // Coarse impedance adjustment
		"IMPED_FINE":      {21, 0b000000000000011111111}, // Fine impedance adjustment
		"PA_FB_R1":        {1, 0b1},                      // Preamp Feedback Resistor 1
		"PA_OUTPUT_DC":    {8, 0b00011111},               // Preamp DC output level
		"SH_PT":           {10, 0b0000011111},            // Shaper Peaking Time
		"ENABLE_50_SH":    {1, 0b0},                      // not sure 0 or 1 for 50 OHM    // Enable 50 Ohm Shaping
		"SH_OUTPUT_DC":    {15, 0b000001111111111},       // Shaper DC output level
		"SH_SUM_PWR":      {1, 0b0},                      // Sum circuit shaper Power
		"SH_SUM_GAIN_CH0": {3, 0b001},                    // Sum circuit gain for CH0
		"SH_SUM_GAIN_CH1": {3, 0b001},                    // Sum circuit gain for CH1
		"SH_SUM_GAIN_CH2": {3, 0b001},                    // Sum circuit gain for CH2
		"SH_SUM_GAIN_CH3": {3, 0b001},                    // Sum circuit gain for CH3
		"SH_SUM_OUTPUT":   {15, 0b000001111111111},       // Sum circuit Output level
		"DACN0_CUR":       {6, 0b000011},                 // DACN0 current
		"DACN1_CUR":       {6, 0b110000},                 // DACN1 current
		"DACP1_CUR":       {6, 0b001111},                 // DACP1 current
		"BGR_SEL":         {2, 0b11},                     // maybe 01    // Bandgap source selection, 11 for external, 01 for internal
		"PA12V_CUR":       {1, 0b1},                      // Preamp 1,2V current
		"SH1V2_CUR":       {1, 0b1},                      // Shaper 1,2V current
		"PA2V5_CUR":       {1, 0b1},                      // Preamp 2.5V current
		"UNUSED":          {5, 0b00000},                  // unused bits
	}

	alfve2_registers_25Ohm = map[string]*RegDescription{
		"CH_PWR":          {4, 0b0000},                   // migh be 1111    // Channel power
		"PA_FB_R0":        {2, 0b01},                     // Feedback Resistor 0
		"PA_FB_C0":        {2, 0b01},                     // feedback capacitor 0
		"PA_OUT_OFFSET":   {4, 0b0001},                   // Preamp Output Offset
		"PA_FB_C1":        {2, 0b01},                     // Preamp Feedback Capacitor 1
		"IMPED_COARSE":    {2, 0b11},                     // Coarse impedance adjustment
		"IMPED_FINE":      {21, 0b000000000000011111111}, // Fine impedance adjustment
		"PA_FB_R1":        {1, 0b1},                      // Preamp Feedback Resistor 1
		"PA_OUTPUT_DC":    {8, 0b00011111},               // Preamp DC output level
		"SH_PT":           {10, 0b0000011111},            // Shaper Peaking Time
		"ENABLE_50_SH":    {1, 0b0},                      // not sure 0 or 1 for 50 OHM    // Enable 50 Ohm Shaping
		"SH_OUTPUT_DC":    {15, 0b000001111111111},       // Shaper DC output level
		"SH_SUM_PWR":      {1, 0b0},                      // Sum circuit shaper Power
		"SH_SUM_GAIN_CH0": {3, 0b001},                    // Sum circuit gain for CH0
		"SH_SUM_GAIN_CH1": {3, 0b001},                    // Sum circuit gain for CH1
		"SH_SUM_GAIN_CH2": {3, 0b001},                    // Sum circuit gain for CH2
		"SH_SUM_GAIN_CH3": {3, 0b001},                    // Sum circuit gain for CH3
		"SH_SUM_OUTPUT":   {15, 0b000001111111111},       // Sum circuit Output level
		"DACN0_CUR":       {6, 0b000011},                 // DACN0 current
		"DACN1_CUR":       {6, 0b110000},                 // DACN1 current
		"DACP1_CUR":       {6, 0b001111},                 // DACP1 current
		"BGR_SEL":         {2, 0b10},                     // maybe 01    // Bandgap source selection, 11 for external, 01 for internal
		"PA12V_CUR":       {1, 0b1},                      // Preamp 1,2V current
		"SH1V2_CUR":       {1, 0b1},                      // Shaper 1,2V current
		"PA2V5_CUR":       {1, 0b1},                      // Preamp 2.5V current
		"UNUSED":          {5, 0b00000},                  // unused bits
	}
	alfve2_registers = alfve2_registers_50Ohm
	return
}

func GenerateI2CRegisters(alfve2_registers map[string]*RegDescription) [][]uint32 {
	alfe2_i2c_map, _, _, _ := GetALFE2RegisterMaps()
	//Bitbanging to pack the logical register in a list of I2C registers
	transactions := make([][]uint32, 0)
	// for each I2C address
	orderedi2ckeys := make([]int, 0)
	for k := range alfe2_i2c_map {
		orderedi2ckeys = append(orderedi2ckeys, k)
	}
	sort.Ints(orderedi2ckeys)

	for _, key := range orderedi2ckeys {
		value := alfe2_i2c_map[key]

		orderedalfekeys := make([]string, 0)
		for k := range value {
			orderedalfekeys = append(orderedalfekeys, k)
		}
		sort.Strings(orderedalfekeys)
		reg_value_bits := make([]int, 0)
		//check the content of this register from mapping,
		//then fetch the required bit from logical registers (in mapping) and assemble the value
		//fmt.Println("Register ", key, " content ", value)
		for _, reg := range orderedalfekeys {
			rangevalue := value[reg]
			//fmt.Printf("Processing reg %v with rangevalue %v, from %v \n", reg, rangevalue, alfve2_registers[reg])
			value_bits := bitfield(alfve2_registers[reg].Value, alfve2_registers[reg].Length)
			value_bits = reverse(value_bits)
			value_bits = value_bits[rangevalue[0]:rangevalue[1]]
			value_bits = reverse(value_bits)
			reg_value_bits = append(value_bits, reg_value_bits...)
			//fmt.Printf("inserting  in %v  %v %v \n", reg, " from ", value_bits)
			//fmt.Println(reg_value_bits)
		}
		transactions = append(transactions, []uint32{uint32(key), uint32(bitlisttoInt(reg_value_bits))})
	}
	//return list of address and value to write
	return transactions
}
