package golpgbtdongle

import (
	"fmt"
	"log"
	"time"
)

const I2CM0CONFIG = 0x100
const I2CM0ADDRESS = 0x101
const I2CM0DATA0 = 0x102
const I2CM0CMD = 0x106
const I2CM0STATUS = 0x171
const I2CM0READBYTE = 0x173

const I2CM1CONFIG = 0x107
const I2CM1ADDRESS = 0x108
const I2CM1DATA0 = 0x109
const I2CM1CMD = 0x10d
const I2CM1STATUS = 0x186
const I2CM1READBYTE = 0x188

const I2CM2CONFIG = 0x10e
const I2CM2ADDRESS = 0x10f
const I2CM2DATA0 = 0x110
const I2CM2CMD = 0x114
const I2CM2STATUS = 0x19b
const I2CM2READBYTE = 0x19d

const I2C_WRITE_CR = 0x0
const I2C_1BYTE_WRITE = 0x2
const I2C_1BYTE_READ = 0x3

const alfe2_i2c_addr = 0x0

const NTRIALS = 1
const WAITTIME = 10 * time.Microsecond

func SetlpGBTMasterAddress(master int) (config, address, data0, cmd, stats, readbyte int) {
	switch master {
	case 0:
		return I2CM0CONFIG, I2CM0ADDRESS, I2CM0DATA0, I2CM0CMD, I2CM0STATUS, I2CM0READBYTE
	case 1:
		return I2CM1CONFIG, I2CM1ADDRESS, I2CM1DATA0, I2CM1CMD, I2CM1STATUS, I2CM1READBYTE
	case 2:
		return I2CM2CONFIG, I2CM2ADDRESS, I2CM2DATA0, I2CM2CMD, I2CM2STATUS, I2CM2READBYTE
	default:
		return I2CM0CONFIG, I2CM0ADDRESS, I2CM0DATA0, I2CM0CMD, I2CM0STATUS, I2CM0READBYTE
	}
}

type i2cer interface {
	Init()
	AddDevice(name string, address uint32, bus string)
	ListDevices()
	Write(name string, register uint8, data []byte) I2CError
	Read(name string, register uint8, length int) ([]byte, I2CError)
}

type Device struct {
	address int
}

type I2CController struct {
	i2cer
	GBTxController *GBTx
	master         int
	devices        map[string]Device
	verbose        bool
}

func (i *I2CController) Init(emulation, verbose bool, master int) {

	i.GBTxController = &GBTx{GBTx_address: 127}
	i.master = master
	i.verbose = verbose
	i.devices = make(map[string]Device)
	i.GBTxController.Init(emulation, verbose)
	//i.devices = make(map[string]Device)

}

type I2CError struct {
	N_sdllow, N_noack int
}

func (err I2CError) Error() string {
	return fmt.Sprintf("SDL Low errors : %v, No Ack errors : %v \n", err.N_sdllow, err.N_noack)
}

func (i *I2CController) SetDeviceAddress(name string, address int) {
	i.devices[name] = Device{address: address}
}

func (i *I2CController) AddDevice(name string, address uint16, bus string) {

	if i.verbose {
		log.Printf("Adding device %v with address %v on bus %v \n", name, address, bus)
	}
	i.devices[name] = Device{address: int(address)}
}

func (i *I2CController) ListDevices() {
	for key, dev := range i.devices {
		log.Printf("Device %v with address %v  \n", key, dev.address)
	}
}

func (i *I2CController) Write(name string, data []byte) I2CError {

	n_noack, n_sdllow := 0, 0
	_, I2CADDRESS, I2CDATA0, I2CCMD, I2CSTATUS, _ := SetlpGBTMasterAddress(i.master)

	i.GBTxController.GBTx_Write_Register(I2CADDRESS, i.devices[name].address&0x7F)
	i.GBTxController.GBTx_Write_Register(I2CDATA0, int(data[0]))
	i.GBTxController.GBTx_Write_Register(I2CCMD, I2C_1BYTE_WRITE)
	time.Sleep(WAITTIME)
	status := i.GBTxController.GBTx_Read_Register(I2CSTATUS)

	trials := 0
	for (status != 0x4) && (trials < NTRIALS) {
		time.Sleep(WAITTIME)
		status = i.GBTxController.GBTx_Read_Register(I2CSTATUS)
		trials += 1
	}

	n_noack = int((status >> 6) & 0b1)
	n_sdllow = int((status >> 3) & 0b1)

	i2cerr := I2CError{N_noack: n_noack, N_sdllow: n_sdllow}

	if (trials == NTRIALS) && (status != 0x4) {
		log.Printf("Error during write transaction no ack %v, sdl low : %v", n_noack, n_sdllow)
		return i2cerr
	} else {
		return i2cerr
	}
}

func (i *I2CController) Read(name string, length int) (data []byte, err I2CError) {
	n_noack, n_sdllow := 0, 0
	_, I2CADDRESS, _, I2CCMD, I2CSTATUS, I2CREADBYTE := SetlpGBTMasterAddress(i.master)

	i.GBTxController.GBTx_Write_Register(I2CADDRESS, i.devices[name].address&0x7F)
	i.GBTxController.GBTx_Write_Register(I2CCMD, I2C_1BYTE_READ)
	time.Sleep(WAITTIME)
	status := i.GBTxController.GBTx_Read_Register(I2CSTATUS)

	trials := 0
	for (status != 0x4) && (trials < NTRIALS) {
		time.Sleep(WAITTIME)
		status = i.GBTxController.GBTx_Read_Register(I2CSTATUS)
		trials += 1
	}

	n_noack = int((status >> 6) & 0b1)
	n_sdllow = int((status >> 3) & 0b1)

	i2cerr := I2CError{N_noack: n_noack, N_sdllow: n_sdllow}

	if (trials == NTRIALS) && (status != 0x4) {
		log.Printf("Error during read transaction no ack %v, sdl low : %v", n_noack, n_sdllow)
		return []byte{0}, i2cerr
	} else {

		data = make([]byte, 1)
		data[0] = i.GBTxController.GBTx_Read_Register(I2CREADBYTE)
		return data, i2cerr
	}
}
