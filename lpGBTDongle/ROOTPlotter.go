package golpgbtdongle

import (
	"go-hep.org/x/hep/groot"
	"go-hep.org/x/hep/groot/rhist"
	"go-hep.org/x/hep/hbook"
)

type ROOTWriter struct {
	File *groot.File
}

type Graph struct {
	X, Y                        []float64
	Title, Xlabel, Ylabel, Name string
}

func (w *ROOTWriter) Write(gr Graph) {

	s := hbook.NewS2D()

	for i, v := range gr.X {
		s.Fill(hbook.Point2D{X: v, Y: gr.Y[i]})
	}
	annotation := s.Annotation()
	annotation["title"] = gr.Title
	rgraph := rhist.NewGraphFrom(s)
	w.File.Put(gr.Name, rgraph)

}
