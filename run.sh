export NTRIALS=50

# ./ALFE2_lpGBT_TestTool_simple -n $NTRIALS -fr 0 -ra=true -rl=true
# ./ALFE2_lpGBT_TestTool_simple -n $NTRIALS -fr 1 -ra=true -rl=true
# ./ALFE2_lpGBT_TestTool_simple -n $NTRIALS -fr 2 -ra=true -rl=true
# ./ALFE2_lpGBT_TestTool_simple -n $NTRIALS -fr 3 -ra=true -rl=true

./ALFE2_lpGBT_TestTool_simple -n $NTRIALS -fr 0 -ra=false -rl=true
./ALFE2_lpGBT_TestTool_simple -n $NTRIALS -fr 1 -ra=false -rl=true
./ALFE2_lpGBT_TestTool_simple -n $NTRIALS -fr 2 -ra=false -rl=true
./ALFE2_lpGBT_TestTool_simple -n $NTRIALS -fr 3 -ra=false -rl=true

./ALFE2_lpGBT_TestTool_simple -n $NTRIALS -fr 0 -ra=true -rl=false
./ALFE2_lpGBT_TestTool_simple -n $NTRIALS -fr 1 -ra=true -rl=false
./ALFE2_lpGBT_TestTool_simple -n $NTRIALS -fr 2 -ra=true -rl=false
./ALFE2_lpGBT_TestTool_simple -n $NTRIALS -fr 3 -ra=true -rl=false