from ROOT import * 
import csv

colors = [kRed,kBlue,kGreen,kCyan]

def ReadFile(filename,freq="1 MHz",rootfile="results.root",N=3000): 

    reader = csv.reader(open(filename)) 

    gr_ackrd = TH1F("Read NoAck Errors,%s"%freq,"Read NoAck Errors,%s"%freq,512,0,512)
    gr_ackrw = TH1F("Write NoAck Errors,%s"%freq,"Write NoAck Errors,%s"%freq,512,0,512)
    gr_sdlrd = TH1F("Read SDLLow Errors,%s"%freq,"Read SDLLow Errors,%s"%freq,512,0,512)
    gr_sdlwr = TH1F("Write SDLLow Errors,%s"%freq,"Write SDLLow Errors,%s"%freq,512,0,512)
    gr_readerr = TH1F("Readback Errors,%s"%freq,"Readback Errors,%s"%freq,512,0,512)

    for row in reader:
        delay = int(row[0])
        N= float(N)
        gr_ackrd.SetBinContent(delay,int(row[1])/N)
        gr_ackrw.SetBinContent(delay,int(row[2])/N)
        gr_sdlrd.SetBinContent(delay,int(row[3])/N)
        gr_sdlwr.SetBinContent(delay,int(row[4])/N)
        gr_readerr.SetBinContent(delay,int(row[5]))


    hs = THStack("hs","Error in I2C Transactions, %s"%freq)

    hs.Add(gr_ackrd)
    hs.Add(gr_ackrw)
    hs.Add(gr_sdlrd)
    hs.Add(gr_sdlwr)
    #hs.Add(gr_readerr)
    can = TCanvas("top")
    hs.Draw("HIST PMC PLC PFC nostack")
    hs.SetMinimum(1e-3)
    hs.SetMaximum(1.1)
    can.SetLogy()
    hs.GetXaxis().SetTitle("Delay (#frac{25 ns}{512})")
    hs.GetXaxis().SetTitleSize(0.03)
    hs.GetXaxis().SetTitleOffset(1.4)
    hs.GetYaxis().SetTitle("Probability of error")
    can.BuildLegend(0.149,0.75,0.49,0.89)
    can.SetWindowSize(1600,1200)
    hs.GetYaxis().SetRangeUser(1e-3,1.1)

    can.SaveAs(rootfile+".png","")


    rootfile=TFile.Open(rootfile+".root","recreate")
    gr_ackrd.Write()
    gr_ackrw.Write()
    gr_sdlrd.Write()
    gr_ackrw.Write()
    gr_readerr.Write()
    hs.Write()
    can.Write()

def main():
    #ReadFile("ALFE2_I2C_Scan_errors_1MHz_both_N3000.csv",freq="1 MHz",rootfile="results_1MHz_both.root")    
    #ReadFile("ALFE2_I2C_Scan_errors400kHz_3000.csv",freq="400 kHz",rootfile="results_400kHz_both.root")
    #ReadFile("ALFE2_I2C_Scan_errors_1MHz_ALFE2OnlyReset.csv",freq="1 MHz",rootfile="results_1MHz_resetalfe2.root",N=10000)
    #ReadFile("ALFE2_I2C_Scan_errors_1MHz_lpGBTOnly_N100.csv",freq="1 MHz",rootfile="results_1MHz_resetlpGBT.root",N=100)
    # ReadFile("ALFE2_I2C_Scan_errors_100kHz_N50_ResetALFE2_ResetlpGBT_2.csv",freq="100 kHz",rootfile="results_100kHz_N50_both",N=50)
    # ReadFile("ALFE2_I2C_Scan_errors_200kHz_N50_ResetALFE2_ResetlpGBT_2.csv",freq="200 kHz",rootfile="results_200kHz_N50_both",N=50)
    # ReadFile("ALFE2_I2C_Scan_errors_400kHz_N50_ResetALFE2_ResetlpGBT_2.csv",freq="400 kHz",rootfile="results_400kHz_N50_both",N=50)
    # ReadFile("ALFE2_I2C_Scan_errors_1MHz_N50_ResetALFE2_ResetlpGBT_2.csv",freq="1 MHz",rootfile="results_1MHz_N50_both",N=50)
    ReadFile("ALFE2_I2C_Scan_errors_100kHz_N500_ResetALFE2.csv",freq="1 MHz",rootfile="results_1MHz_N500_alfe2",N=500)
    ReadFile("ALFE2_I2C_Scan_errors_100kHz_N500_ResetlpGBT.csv",freq="1 MHz",rootfile="results_1MHz_N500_lpgbt",N=500)


if __name__ == "__main__":
    main()
