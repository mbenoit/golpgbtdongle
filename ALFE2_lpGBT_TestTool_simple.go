//go:build linux || darwin
// +build linux darwin

package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"math/rand"
	_ "net/http/pprof"
	"os"
	"runtime"
	"runtime/pprof"

	"go-hep.org/x/hep/groot"

	"BNL.gov/golpgbtdongle"
)

func main() {

	logFile, err := os.OpenFile("log.txt", os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		panic(err)
	}
	mw := io.MultiWriter(os.Stdout, logFile)
	log.SetOutput(mw)

	f, err := groot.Create("results.root")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	rwr := golpgbtdongle.ROOTWriter{File: f}

	devaddr := flag.Int("a", 0x0, "I2C base address of the device")
	i2cfreq := flag.Int("fr", 0x3, "I2C frequency, 0=100kHz,1=200kHz,2=400kHz,3=1MHz")
	conffile := flag.String("f", "ALFE2.yaml", "CSV file containing configuration for the clock chip")
	dryrun := flag.Bool("dry", false, "Dry run reading yaml file")
	verbose := flag.Bool("v", false, "print extra verbosity during execution")
	//	readflag := flag.Bool("r", true, "Readback ALFE2 Configuration")
	ntrials := flag.Int("n", 10, "number of configuration trials per delay")
	clock := flag.Int("c", 1, "PS clock to be used")
	master := flag.Int("m", 0, "lpGBT I2C Master to be used")
	cpuprofile := flag.String("cpuprofile", "", "write cpu profile to `file`")
	memprofile := flag.String("memprofile", "", "write memory profile to `file`")
	startdelay := flag.Int("s", 0, "start delay")
	resetalfe := flag.Bool("ra", true, "reset alfe on error")
	resetlpgbt := flag.Bool("rl", true, "reset lpgbt on error")

	flag.Parse()

	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		defer f.Close() // error handling omitted for example
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal("could not start CPU profile: ", err)
		}
		defer pprof.StopCPUProfile()
	}

	if *dryrun {
		log.Println("WARNING!!! This is a dry-run !!")
	}

	log.Printf("Using clock PS%v and I2C Master M%v \n", *clock, *master)

	devName := "0"
	ctrl := golpgbtdongle.NewALFE2Controller(devName, *devaddr, *master, *dryrun, *verbose)
	log.Println("Connecting I2C Level translator...")
	err = ctrl.I2cctrl.GBTxController.Dongle.I2CConnect(true)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Reseting I2C Controller ...")
	ctrl.I2cctrl.GBTxController.Dongle.I2CReset()
	log.Println("Scanning for I2C devices from 0 to 127...")
	slaves, _ := ctrl.I2cctrl.GBTxController.Dongle.I2CScan(0, 127)
	log.Println(slaves)
	log.Println("Ready for operation...")

	ctrl.ReadConfiguration(*conffile)

	ctrl.I2cctrl.GBTxController.GBTx_Dump_Config("lpGBTv1.conf")
	ctrl.SetI2CClockSpeed(uint8(*i2cfreq))

	freq := ""
	switch *i2cfreq {
	case 0:
		freq = "100kHz"
	case 1:
		freq = "200kHz"
	case 2:
		freq = "400kHz"
	case 3:
		freq = "1MHz"
	}

	resettype := ""
	switch *resetalfe {
	case true:
		resettype += "_ResetALFE2"
	}
	switch *resetlpgbt {
	case true:
		resettype += "_ResetlpGBT"
	}
	ctrl.I2cctrl.GBTxController.SetPSClockSpeed(1, 1)
	ctrl.I2cctrl.GBTxController.SetPSClockDrive(7, 1)

	ctrl.SetClockDelay(0, *clock, true)

	//Set reset
	ctrl.I2cctrl.GBTxController.SetGPIOOutputDrive(5, true, true)
	ctrl.I2cctrl.GBTxController.SetGPIODirection(5, true)
	ctrl.ResetALFE2(5)
	ctrl.I2cctrl.GBTxController.ResetI2CMasters()

	csvFile, err := os.Create(fmt.Sprintf("ALFE2_I2C_Scan_errors_%v_N%v%v_2.csv", freq, *ntrials, resettype))

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}
	defer csvFile.Close()

	csvwriter := csv.NewWriter(csvFile)
	defer csvwriter.Flush()

	Xdata := make([]float64, 512)
	sdlowreads := make([]float64, 512)
	sdlowwrites := make([]float64, 512)
	ackreads := make([]float64, 512)
	ackwrites := make([]float64, 512)
	readbackerrors := make([]float64, 512)

	for delay := *startdelay; delay < 512; delay += 1 {
		Xdata[delay] = float64(delay) * (25.0 / 512.0)
		noackrt, sdllowrt, noackwt, sdllowwt, readbackt := 0, 0, 0, 0, 0

		log.Printf("Setting ALFE2 I2C driver clock delay to %v \n", delay)
		ctrl.SetClockDelay(uint16(delay), *clock, true)
		ctrl.ResetALFE2(5)
		ctrl.I2cctrl.GBTxController.ResetI2CMasters()
		ctrl.SetI2CClockSpeed(uint8(*i2cfreq))

		for trials := 0; trials < *ntrials; trials++ {
			noackr, sdllowr, noackw, sdlloww, readback := 0, 0, 0, 0, 0

			current_value := rand.Intn(100)
			//ctrl.IncrementConfiguration()
			//noackt, sdllowt := ctrl.Configure(*verbose, *dryrun)
			errors := make([]golpgbtdongle.I2CError, 1)
			log.Printf("Writing %x", current_value)

			errors[0] = ctrl.I2cctrl.Write(devName+"R2", []byte{byte(current_value)})
			for _, err := range errors {
				noackw += err.N_noack
				sdlloww += err.N_sdllow
			}

			if noackw > 0 || sdlloww > 0 {
				if *resetalfe {
					ctrl.ResetALFE2(5)
				}
				if *resetlpgbt {
					ctrl.I2cctrl.GBTxController.ResetI2CMasters()
					ctrl.SetI2CClockSpeed(uint8(*i2cfreq))

				}
				log.Printf("Writing , %x noack, %v sdl low", noackw, sdlloww)
			}

			var data []byte
			data, errors[0] = ctrl.I2cctrl.Read(devName+"R2", 1)

			for _, err := range errors {
				noackr += err.N_noack
				sdllowr += err.N_sdllow

			}
			if noackr > 0 || sdllowr > 0 {
				if *resetalfe {
					ctrl.ResetALFE2(5)
				}
				if *resetlpgbt {
					ctrl.I2cctrl.GBTxController.ResetI2CMasters()
					ctrl.SetI2CClockSpeed(uint8(*i2cfreq))
				}
				log.Printf("Reading , %v noack, %v sdl low", noackr, sdllowr)
			}
			log.Printf("Reading %x expected %x", data[0], byte(current_value))

			if data[0] != byte(current_value) {
				readback += 1
			}
			noackrt += noackr
			noackwt += noackw
			sdllowrt += sdllowr
			sdllowwt += sdlloww
			readbackt += readback
		}

		sdlowreads[delay] = float64(sdllowrt)
		sdlowwrites[delay] = float64(sdllowwt)
		ackreads[delay] = float64(noackrt)
		ackwrites[delay] = float64(noackwt)
		readbackerrors[delay] = float64(readbackt)

		log.Printf("%v No Ack incident, %v sdl low incident, %v readback errors during configuration \n", noackrt+noackwt, sdllowrt+sdllowwt, readbackt)
		reccord := []string{fmt.Sprintf("%v", delay), fmt.Sprintf("%v", noackwt), fmt.Sprintf("%v", noackrt), fmt.Sprintf("%v", sdllowwt), fmt.Sprintf("%v", sdllowrt), fmt.Sprintf("%v", readbackt)}
		csvwriter.Write(reccord)
		csvwriter.Flush()
	}

	Graph := golpgbtdongle.Graph{X: Xdata, Y: sdlowreads, Xlabel: "Delay (ns)", Ylabel: "Number of Read SDL Low errors", Name: "SDL LOw on Read"}
	rwr.Write(Graph)
	Graph = golpgbtdongle.Graph{X: Xdata, Y: sdlowwrites, Xlabel: "Delay (ns)", Ylabel: "Number of Write SDL Low errors", Name: "SDL Low on Write"}
	rwr.Write(Graph)
	Graph = golpgbtdongle.Graph{X: Xdata, Y: ackreads, Xlabel: "Delay (ns)", Ylabel: "Number of Read No Ack errors", Name: "No Ack on Read"}
	rwr.Write(Graph)
	Graph = golpgbtdongle.Graph{X: Xdata, Y: ackwrites, Xlabel: "Delay (ns)", Ylabel: "Number of Write No Ack errors", Name: "No Ack on Write"}
	rwr.Write(Graph)
	Graph = golpgbtdongle.Graph{X: Xdata, Y: readbackerrors, Xlabel: "Delay (ns)", Ylabel: "Number of readback errors", Name: "wrong readback"}
	rwr.Write(Graph)

	if *memprofile != "" {
		f, err := os.Create(*memprofile)
		if err != nil {
			log.Fatal("could not create memory profile: ", err)
		}
		defer f.Close() // error handling omitted for example
		runtime.GC()    // get up-to-date statistics
		if err := pprof.WriteHeapProfile(f); err != nil {
			log.Fatal("could not write memory profile: ", err)
		}
	}
}
